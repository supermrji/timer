//timer
function Timer(e, t) {
    var n, r, i = t;
    this.pause = function () {
        window.clearTimeout(n);
        i -= new Date - r
    };
    this.resume = function () {
        r = new Date;
        n = window.setTimeout(e, i);
        return i
    };
    this.stop = function () {
        window.clearTimeout(n)
    };
    this.resume()
}
